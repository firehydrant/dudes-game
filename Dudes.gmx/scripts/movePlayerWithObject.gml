///movePlayerWithObject(directionToMove, distance)

if (argument1 != 0) {
  with (obj_character) {
    if (instance_place(x, y + 1, other.object_index) == other.id) {
      y -= 1;
      move_contact_solid(argument0, argument1);
      y += 1;
    }
  }
}
