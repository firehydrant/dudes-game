/// moveUp()

//Last Edit 11-7-15

//Checks to see if the up button or key was pressed

return (checkPauseMenuButtons(UP_BUTTON_X, 
                            UP_BUTTON_Y, 
                            UP_BUTTON_X + sprite_get_width(spr_up), 
                            UP_BUTTON_Y + sprite_get_height(spr_up), 
                            device_mouse_check_button(0, mb_left)) ||
      keyboard_check(vk_up) ||
      keyboard_check(ord('S')));
