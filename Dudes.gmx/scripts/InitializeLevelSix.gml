/// InitializeLevelSix()

numCharsInLevel = 6;
charSpawnX = 325;
charSpawnY = 64;




//Break Ground (Each bunch goes from the top down from the level)
InitializeBreakGround(725, 432);
InitializeBreakGround(900, 376);
InitializeBreakGround(1075, 312);

InitializeBreakGround(1000, 800);
InitializeBreakGround(1080, 800);

InitializeBreakGround(625, 1448);
InitializeBreakGround(705, 1448);
InitializeBreakGround(785, 1448);
InitializeBreakGround(975, 1448);
InitializeBreakGround(1055, 1448);
InitializeBreakGround(1250, 1448);
InitializeBreakGround(1330, 1448);
InitializeBreakGround(1410, 1448);
InitializeBreakGround(1490, 1448);
InitializeBreakGround(1570, 1448);

//Only movable block
instance_create(1167, 924, obj_movable_block);

//First horizontal door
InitializeButton(1326, 500, 1525, 432, spr_horizontal_door, DIR_UP);
//Second horizontal door
InitializeButton(1425, 1660, 1750, 1680, spr_horizontal_door, DIR_DOWN);

//First vertical door
InitializeButton(1525, 2088, 1351, 2124, spr_vertical_door, DIR_LEFT);
//Second vertical door
InitializeButton(584, 2092, 1025, 1920, spr_vertical_door, DIR_LEFT);

//Flippers (Left to Right)
InitializeFlipper(648, 1184, true);
InitializeFlipper(851, 1184, true);
InitializeFlipper(1198, 1184, true);

//Only movable ground
InitializeMoveGround(250, 1680, true);
