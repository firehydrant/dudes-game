/// setUpSavedCharacter(playerAction)

if (argument0 == PLAYER_MOVE_BOUNCE_NONE) {
  movedLeft = false;
  movedRight = false;
  movedUp = false;
  movedDown = false;
  movedBounce = time[currActionPlace];
  playerActionCountdown = 1;
  show_debug_message("Bounce none " + string(id));
}
else if (argument0 == PLAYER_MOVE_BOUNCE_LEFT) {
  movedLeft = true;
  movedRight = false;
  movedUp = false;
  movedDown = false;
  movedBounce = time[currActionPlace];
  playerActionCountdown = 1;
  show_debug_message("Bouncing left");
}
else if (argument0 == PLAYER_MOVE_BOUNCE_RIGHT) {
  movedLeft = false;
  movedRight = true;
  movedUp = false;
  movedDown = false;
  movedBounce = time[currActionPlace];
  playerActionCountdown = 1;
  show_debug_message("Bouncing right");
}
else if (argument0 == PLAYER_MOVE_LEFT_DOWN) {
  movedLeft = true;
  movedRight = false;
  movedUp = false;
  movedDown = true;
  movedBounce = 0;
}
else if (argument0 == PLAYER_MOVE_LEFT_UP) {
  movedLeft = true;
  movedRight = false;
  movedUp = true;
  movedDown = false;
  movedBounce = 0;
}
else if (argument0 == PLAYER_MOVE_RIGHT_DOWN) {
  movedLeft = false;
  movedRight = true;
  movedUp = false;
  movedDown = true;
  movedBounce = 0;
}
else if (argument0 == PLAYER_MOVE_RIGHT_UP) {
  movedLeft = false;
  movedRight = true;
  movedUp = true;
  movedDown = false;
  movedBounce = 0;
}
else if (argument0 == PLAYER_MOVE_LEFT) {
  movedLeft = true;
  movedRight = false;
  movedUp = false;
  movedDown = false;
  movedBounce = 0;
}
else if (argument0 == PLAYER_MOVE_RIGHT) {
  movedLeft = false;
  movedRight = true;
  movedUp = false;
  movedDown = false;
  movedBounce = 0;
}
else if (argument0 == PLAYER_MOVE_DOWN) {
  movedLeft = false;
  movedRight = false;
  movedUp = false;
  movedDown = true;
  movedBounce = 0;
}
else if (argument0 == PLAYER_MOVE_UP) {
  movedLeft = false;
  movedRight = false;
  movedUp = true;
  movedDown = false;
  movedBounce = 0;
}
else {
  // Player is not moving
  movedLeft = false;
  movedRight = false;
  movedUp = false;
  movedDown = false;
  movedBounce = 0;
}

movedDown = checkIfCanStand(movedDown, spr_char_idle);
