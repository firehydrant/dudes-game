/// canClimbFlipper()

var inst = instance_place(x + 1, y, obj_flipper);

if (inst == noone) {
  inst = instance_place(x - 1, y, obj_flipper);
}

if (inst != noone) {
  if (inst.down){
    return true;
  }
}
else{
  return false;
}
