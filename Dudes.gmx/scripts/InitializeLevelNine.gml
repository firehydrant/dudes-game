///InitializeLevelNine()

//shortest path is 6(can be shorter possible)
numCharsInLevel = 7;

charSpawnX = 350;
charSpawnY = 167;

/* Break Ground Start */

//Bridge of two break ground
InitializeBreakGround(900, 232);
InitializeBreakGround(980, 232);

//Stack
InitializeBreakGround(1600, 472);
InitializeBreakGround(1600, 492);
InitializeBreakGround(1600, 512);
InitializeBreakGround(1600, 532);
InitializeBreakGround(1600, 552);
InitializeBreakGround(1600, 572);
InitializeBreakGround(1600, 592);
InitializeBreakGround(1600, 612);

/* Break Ground End */

//First vertical door
InitializeButton(1475, 212, 1175, 248, spr_vertical_door, DIR_LEFT);
//Second vertical door
InitializeButton(1573, 916, 1675, 952, spr_vertical_door, DIR_LEFT);

//First horizontal door set (left side)
InitializeButton(501, 764, 225, 856, spr_horizontal_door, DIR_DOWN);
//Second horizontal door set (right side)
InitializeButton(2058, 1084, 1794, 1072, spr_horizontal_door, DIR_DOWN);

//First move ground from top to bottom
InitializeMoveGround(358, 610, true);
//Second move ground from top to bottom
InitializeMoveGround(1895, 764, true);
//Third move ground from top to bottom
InitializeMoveGround(233, 1024, true);

//Only movable block
instance_create(675, 376, obj_movable_block);
