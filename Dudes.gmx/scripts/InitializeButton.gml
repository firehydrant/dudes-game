/// InitializeButton(buttonX, buttonY, associatedX, associatedY, associatedSprite, direction)
var button_inst = instance_create(argument0, argument1, obj_button_weight);

with (button_inst) {
  associated_inst = InitializeDisappearDoor(argument2, argument3, argument4, argument5);
}
