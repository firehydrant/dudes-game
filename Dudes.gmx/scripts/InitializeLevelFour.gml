///InitializeLevelFour()

//shortest path is 4
numCharsInLevel = 7;

charSpawnX = 94;
charSpawnY = 246;

//First move ground
InitializeMoveGround(250, 356, true);
//Last move ground
InitializeMoveGround(3162, 600, false);

//First vertical door
InitializeButton(425, 164, 500, 248, spr_vertical_door, DIR_LEFT);
//Second vertical door
InitializeButton(1303, 484, 1150, 520, spr_vertical_door, DIR_LEFT);

//Only horizontal door
InitializeButton(2734, 484, 2975, 504, spr_horizontal_door, DIR_UP);

//First flipper 
InitializeFlipper(1845, 264, true);
//Second Flipper
InitializeFlipper(2233, 264, false);

//Only movable block
instance_create(2075, 404, obj_movable_block);
