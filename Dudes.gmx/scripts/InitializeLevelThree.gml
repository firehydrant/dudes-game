///InitializeLevelThree()

numCharsInLevel = 7;
charSpawnX = 1031;
charSpawnY = 95;

//First movable ground
InitializeMoveGround(985, 432, false);
//Second movable ground
InitializeMoveGround(985, 1136, false);

//First button down the tunnel
InitializeButton(704, 676, 975, 696, spr_horizontal_door, DIR_UP);
//Second button down the tunnel
InitializeButton(656, 1372, 975, 1168, spr_horizontal_door, DIR_UP);
//Third button down the tunnel
InitializeButton(1011, 1828, 975, 1604, spr_horizontal_door, DIR_UP);

//Only Vertical door in level
InitializeButton(1344, 1180, 955, 1048, spr_vertical_door, DIR_LEFT);

//Weighted Ground
instance_create(1570, 432, obj_weighted_ground);

//Flipper
InitializeFlipper(373, 1792, true);
