/// moveRight()

//Last Edit 11-7-15

//Checks to see if the right button or key was pressed

return (checkPauseMenuButtons(RIGHT_BUTTON_X, 
                            RIGHT_BUTTON_Y, 
                            RIGHT_BUTTON_X + sprite_get_width(spr_right), 
                            RIGHT_BUTTON_Y + sprite_get_height(spr_right), 
                            device_mouse_check_button(0, mb_left)) ||
      keyboard_check(vk_right) ||
      keyboard_check(ord('D')));
