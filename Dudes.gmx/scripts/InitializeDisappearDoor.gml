/// InitializeDisappearDoor(x, y, sprite, direction)
var associated_inst = instance_create(argument0, argument1, obj_disappear_door);
associated_inst.sprite_index = argument2;
associated_inst.dir = argument3;

if (argument3 == DIR_RIGHT) {
  // Flip door sprite
  associated_inst.image_xscale = -1;
  associated_inst.x += associated_inst.sprite_width;
}

return associated_inst;
