/// moveLeft()

//Last Edit 11-7-15

//Checks to see if the left button or key was pressed

return (checkPauseMenuButtons(LEFT_BUTTON_X, 
                            LEFT_BUTTON_Y, 
                            LEFT_BUTTON_X + sprite_get_width(spr_left), 
                            LEFT_BUTTON_Y + sprite_get_height(spr_left), 
                            device_mouse_check_button(0, mb_left)) ||
      keyboard_check(vk_left) ||
      keyboard_check(ord('A')));
