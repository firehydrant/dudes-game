///viewCheckBounds()

//Fuction is called whenever the view is changed 
//Aka when Scrolling or zooming
//To keep the view within the boundsaries of the room.

// Handle X Direction Bounds
if (view_xview[0] < 0) {
  view_xview[0] = 0;
}
else if (view_xview[0] > room_width - view_wview[0]) {
  view_xview[0] = room_width - view_wview[0];
}

// Handle Y Direction Bounds
if (view_yview[0] < 0) {
  view_yview[0] = 0;
}
else if (view_yview[0] > room_height - view_hview[0]) {
  view_yview[0] = room_height - view_hview[0];
}
