/// InitializeLevelFive()

numCharsInLevel = 6;
charSpawnX = 3743;
charSpawnY = 265;

//Everything is coded right to left


//First vertical door
InitializeButton(4175, 180, 4075, 100, spr_vertical_door, DIR_LEFT);
//Second vertical door
InitializeButton(2799, 428, 2755, 100, spr_vertical_door, DIR_LEFT);
//Third vertical door
InitializeButton(1175, 1408, 1376, 1444, spr_vertical_door, DIR_RIGHT);

//Only horizontal door
InitializeButton(299, 1636, 125, 1784, spr_horizontal_door, DIR_DOWN);

//First movable ground
InitializeMoveGround(3700, 330, true);
//Second movable ground
InitializeMoveGround(1900, 1176, false);

//Only movable block
instance_create(2974, 228, obj_movable_block);

//Only weighted ground
instance_create(745, 1656, obj_weighted_ground);
