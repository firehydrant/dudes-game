///animateCharacter(bool CROUCH, bool isMovingRight, bool isMovingLeft)

//Last Edit 10-4-14
//Handles all characters animation according to the movement keys that were pressed

// Assume normal speed by default and can change later
//due to crouching
player_speed = PLAYER_WALK_SPEED;
  
// Choose direction to face
if (argument1) {
  image_xscale = 1;
}
else if (argument2) {
  image_xscale = -1;
}
if (place_meeting(x, y + 1, obj_solid_parent)) {

  if (argument0 && place_meeting(x, y + 1, obj_solid_parent) && !place_meeting(x, y, obj_water)) {
    player_speed = PLAYER_CROUCH_SPEED;
    if (sprite_index == spr_char_crouching && image_index == 3) {
      sprite_index = spr_char_crouch_idle
    }
    else if (sprite_index != spr_char_crouch_idle) {
      sprite_index = spr_char_crouching;
    }
  }
  else if (argument1 || argument2){
    sprite_index = spr_char_run;
  }
  else {
    sprite_index = spr_char_idle;
  }
}
else if (vspeed > 0) {
  checkIfCanStand(false, spr_char_fall)
}
else {
  sprite_index = spr_char_jump;
}
