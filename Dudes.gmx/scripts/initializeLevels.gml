/// initializeLevels
if (isNewRound) {
  show_debug_message("New round");
  
  // Clear the world of any objects we will respawn
  clearWorld();
  
  switch (global.LEVEL_SELECTED) {
    case LEVEL_ONE:
      InitializeLevelOne();
      break;
    case LEVEL_TWO:
      InitializeLevelTwo();
      break;
    case LEVEL_THREE:
      InitializeLevelThree();
      break;
    case LEVEL_FOUR:
      InitializeLevelFour();
      break;
    case LEVEL_FIVE:
      InitializeLevelFive();
      break;
    case LEVEL_SIX:
      InitializeLevelSix();
      break;
    case LEVEL_SEVEN:
      InitializeLevelSeven();
      break;
    case LEVEL_EIGHT:
      InitializeLevelEight();
      break;
    case LEVEL_NINE:
      InitializeLevelNine();
      break;
    case LEVEL_TEST:
    default:
      InitializeTestLevel();
      break;
  }
  
  // Spawn all characters needed
  InitializePlayerSpawning(currCharacterPlace, numCharsInLevel, charSpawnX, charSpawnY);
  
  // Is no longer a new round
  isNewRound = false;
}
