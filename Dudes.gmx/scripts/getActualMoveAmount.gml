/// getMoveToCollisionAmount(objectToCheck, attemptedMoveAmount, optioinalPreviousMinAmount)
if (argument1 == 0) {
  return 0;
}

absMoveAmount = abs(argument1);

if (argument_count > 1) {
  argument2 = argument1;
}

var increaseAmount = 4;

for (var i = 0; i <= absMoveAmount; i += increaseAmount) {
  if (argument1 > 0) {
    if (place_meeting(x, y + i + 1, argument0)) {
      if (i < argument2) {
        return i;
      }
    }
    else {
      i -= increaseAmount;
      increaseAmount /= 2;
      if (increaseAmount < 1) {
        return -i;
      }
    }
  }
  else {
    if (place_meeting(x, y - i - 1, argument0)) {
      if (-i > argument2) {
        return -i;
      }
    }
    else {
      i -= increaseAmount;
      increaseAmount /= 2;
      if (increaseAmount < 1) {
        return -i;
      }
    }
  }
}
