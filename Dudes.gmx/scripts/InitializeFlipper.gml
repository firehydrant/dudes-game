///InitializeFlipper(x, y, reversed)

if (argument2){
  var flipper_inst1 = instance_create(argument0 - 100, argument1, obj_flipper);
  
  flipper_inst1.reversed = argument2;
}
else{
  var flipper_inst2 = instance_create(argument0 , argument1, obj_flipper);
  
  flipper_inst2.reversed = argument2;
}
