///moveDown()

//Last Edit 11-7-15

//Checks to see if the down button or key was pressed 

var isCrouching = (checkPauseMenuButtons(DOWN_BUTTON_X, 
                                        DOWN_BUTTON_Y, 
                                        DOWN_BUTTON_X + sprite_get_width(spr_down), 
                                        DOWN_BUTTON_Y + sprite_get_height(spr_down), 
                                        device_mouse_check_button(0, mb_left)) ||
                  keyboard_check(vk_down) ||
                  keyboard_check(ord('S')));

return checkIfCanStand(isCrouching, spr_char_idle); 
