///InitializeLevelSeven()

//shortest path is 4
numCharsInLevel = 7;

charSpawnX = 1693;
charSpawnY = 2127;

/* Break Ground Start*/
//Steps up from the bottom going up
InitializeBreakGround(1900, 2136);
InitializeBreakGround(1750, 2064);
InitializeBreakGround(1900, 1992);
InitializeBreakGround(1750, 1920);
InitializeBreakGround(1900, 1848);
InitializeBreakGround(2050, 1776);
InitializeBreakGround(1900, 1704);

//Bridge that leads into steps
InitializeBreakGround(1950, 1392);
InitializeBreakGround(2030, 1392);
InitializeBreakGround(2110, 1392);
InitializeBreakGround(2200, 1336);
InitializeBreakGround(2250, 1296);
InitializeBreakGround(2300, 1256);
InitializeBreakGround(2150, 1208);
InitializeBreakGround(2100, 1168);
InitializeBreakGround(2050, 1128);
InitializeBreakGround(2000, 1088);
InitializeBreakGround(1950, 1048);

InitializeBreakGround(2025, 784);
InitializeBreakGround(2175, 728);
/* Break Ground End*/

//First move ground
InitializeMoveGround(1470, 2192, false);
//Second move ground
InitializeMoveGround(507, 1624, true);
//Third move ground
InitializeMoveGround(240, 392, true);

//First horizontal door set (lone door)
InitializeButton(1197, 1500, 1625, 1297, spr_horizontal_door, DIR_DOWN);
//Second horizontal door set (short path)
InitializeButton(971, 1044, 1158, 949, spr_horizontal_door, DIR_UP);
//Third horizontal door set (short path)
InitializeButton(1493, 516, 1158, 640, spr_horizontal_door, DIR_DOWN);

//Only vertical door
InitializeButton(632, 1907, 275, 1880, spr_vertical_door, DIR_LEFT);

//Only movable block
instance_create(1475, 652, obj_movable_block);

//Only weighted ground
instance_create(925, 392, obj_weighted_ground);
