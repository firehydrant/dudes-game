/// InitializeLevelOne()

numCharsInLevel = 7;
charSpawnX = 520;
charSpawnY = 330;

//Fourth button down
InitializeButton(440, 1020, 320, 1040, spr_horizontal_door, DIR_UP);
//Third button down 
InitializeButton(640, 920, 700, 680, spr_vertical_door, DIR_LEFT);
//First button down
InitializeButton(440, 520, 300, 440, spr_vertical_door, DIR_LEFT);
//Second button down
InitializeButton(180, 520, 520, 540, spr_horizontal_door, DIR_UP);

//Right flipper
InitializeFlipper(780, 400, false);
//Left Flipper
InitializeFlipper(160, 420, true);

instance_create(140, 1220, obj_weighted_ground);
