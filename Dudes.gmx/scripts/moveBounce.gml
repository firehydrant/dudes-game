/// moveBounce(canRebounce)
if (argument0) {
  var shouldBounce = 0;
  
  if (vspeed > 0) {
    with (obj_character) {
      if (id != other.id) {
        var mX1 = x + sprite_get_bbox_left(sprite_index);
        var mY1 = y + sprite_get_bbox_top(sprite_index);
        var mX2 = x + sprite_get_bbox_right(sprite_index);
        var mY2 = y + sprite_get_bbox_bottom(sprite_index);
        
        var oX1 = other.x + sprite_get_bbox_left(other.sprite_index);
        var oY1 = other.y + sprite_get_bbox_top(other.sprite_index);
        var oX2 = other.x + sprite_get_bbox_right(other.sprite_index);
        var oY2 = other.y + sprite_get_bbox_bottom(other.sprite_index);
        
        var rect_before = rectangle_in_rectangle(mX1, mY1, mX2, mY2, oX1, oY1, oX2, oY2);
        var rect_after = rectangle_in_rectangle(mX1, mY1, mX2, mY2, oX1, oY1 + other.vspeed, oX2, oY2 + other.vspeed);
        
        //if the current player will be falling on another ghost and the current action 
        //of the ghost is negative, it means current character is allowed to bounce
        //on him, otherwise no       
        if (rect_before == 0 && rect_after != 0 && action[currActionPlace] < 0) {
          shouldBounce = player_char_bounce_vspeed;
        }
      }
    }
  }
  
  return shouldBounce;
}
else {
  return false;
}
