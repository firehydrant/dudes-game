///characterMoveBlock(int player_speed)

//Last Edit 10-6-14
//Moves the block according to the distance passed in and its surroundings

var block_inst = instance_place(x + argument0, y, obj_movable_block);

if (block_inst != noone && place_meeting(x, y + 1, obj_solid_parent)){
  var player_block_distance = 0;
  // Get how far the player is from the moveable block
  if (argument0 > 0) {
    player_block_distance = 1;
    
    while (!place_meeting(x + player_block_distance, y, obj_movable_block)) {
      player_block_distance += 1;
    }
  }
  else if (argument0 < 0) {
    player_block_distance = -1;
    
    while (!place_meeting(x + player_block_distance, y, obj_movable_block)) {
      player_block_distance -= 1;
    }
  }
  
  // Get difference to determine how far the block should move
  var block_move = argument0 - player_block_distance;
  
  with (block_inst) {
    moveBlock(block_move);
  }
}
