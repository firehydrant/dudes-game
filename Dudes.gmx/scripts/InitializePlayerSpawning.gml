/// InitializePlayerSpawning(numCharSpawn, numCharInLevel, charSpawnX, charSpawnY)

// Remove old players
with (obj_character) {
  show_debug_message("Removing " + string(id));
  instance_destroy();
}

// Spawn all old players and give them their actions
for (i = 0; i < argument0; i += 1) {
  var oldCharInst = instance_create(argument2, argument3, obj_character);
  show_debug_message("Spawn char  " + string(oldCharInst) + " at index " + string(i));
  oldCharInst.playerStatus = PLAYER_STATE_GHOST;
  oldCharInst.currActionPlace = 0;
  oldCharInst.time = timeAllChars[i];
  oldCharInst.action = actionAllChars[i];
  show_debug_message("Time = " + string(oldCharInst.time));
  show_debug_message("Action = " + string(oldCharInst.action));
}

// Spawn current character
if (argument0 < argument1) {
  currCharInst = instance_create(argument2, argument3, obj_character);
  canRebounce = true;
  
  with (currCharInst) {
    playerStatus = PLAYER_STATE_IN_ACTION;
  }
  
  //Attach view to follow the current players intance
  view_object[0] = currCharInst;
}
else {
  // Game over
  //TODO: Add end level code, for now just go to play screen
  room_goto(rm_main_menu);
}

// Initialze variables for restart
currActionPlace = 0;

// Delete array
singleTime = 0;
singleAction = 0;

singleTime[currActionPlace] = 0;
singleAction[currActionPlace] = PLAYER_MOVE_NONE;
