///pausing()

//Last Edit 7-23-15
//Checks to see if the pause button or key was pressed 

//Reason why it is splicitly stated what sprite height and width
//we are calling for is since the only object that calls this fuction is the 
//Pause obj which has the sprite we need

return (checkPauseMenuButtons(PAUSE_BUTTON_X, 
                            PAUSE_BUTTON_Y, 
                            PAUSE_BUTTON_X + sprite_width, 
                            PAUSE_BUTTON_Y + sprite_height, 
                            mouse_check_button_released(mb_left)) ||
      keyboard_check_released(vk_space));
