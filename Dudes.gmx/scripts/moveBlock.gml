///moveBlock(distance_to_move)

if (!place_meeting(x + argument0, y, obj_solid) && !place_meeting(x + argument0, y, obj_character)) {
  x += argument0;
  
  //Move the Character if there is one on top as well
  if (argument0 > 0){
    movePlayerWithObject(0, argument0);
  }
  else{
    movePlayerWithObject(180, abs(argument0));
  }
}
else {
  var abs_block_move = abs(argument0);
  for (var i = 0; i < abs_block_move; i += 1) {
    if (argument0 > 0) {
      if (place_meeting(x + i, y, obj_solid) || place_meeting(x + i, y, obj_character)) {
        if (i != 0) {
          x += (i - 1);
          
          //Move the Character if there is one on top as well
          movePlayerWithObject(0, i - 1);
        }
        
        break;
      }
    }
    else if (argument0 < 0) {
      if (place_meeting(x - i, y, obj_solid) || place_meeting(x - i, y, obj_character)) {
        if (i != 0) {
          x -= (i - 1);
          
          //Move the Character if there is one on top as well
          movePlayerWithObject(180, i - 1);
        }
        
        break;
      }
    }
    else {
      break;
    }
  }
}
