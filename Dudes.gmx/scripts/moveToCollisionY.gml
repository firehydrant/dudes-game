/// moveToCollisionY(objectToCheck, attemptedMoveAmount)
if (argument1 == 0) {
  return 0;
}

absMoveAmount = abs(argument1);
var lastGoodPosition = 0;

for (var i = 0; i <= absMoveAmount; i += 1) {
  if (argument1 > 0) {
    if (!place_meeting(x, y + i, argument0)) {
      lastGoodPosition = i;
    }
  }
  else {
    if (!place_meeting(x, y - i, argument0)) {
      lastGoodPosition = -i;
    }
  }
}

y += lastGoodPosition;

return lastGoodPosition;
