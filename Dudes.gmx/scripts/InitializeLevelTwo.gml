/// InitializeLevelTwo()

numCharsInLevel = 7;
charSpawnX = 206;
charSpawnY = 309;

//First button next to player
InitializeButton(325, 356, 473, 159, spr_vertical_door, DIR_LEFT);
//Second button up from player
InitializeButton(325, 239, 474, 44, spr_vertical_door, DIR_LEFT);
//Third button from player
InitializeButton(850, 588, 688, 388, spr_vertical_door, DIR_LEFT);
//Only Horizontal button
InitializeButton(1464, 732, 1082, 632, spr_horizontal_door, DIR_UP);

//First movable block
instance_create(445, 539, obj_movable_block);
//Second movable block
instance_create(875, 840, obj_movable_block);
