///movePlayer(bool LEFT, bool RIGHT, bool UP, bool DOWN, int BOUNCE)

//Last Edit 5-2-14
//Script is in charge or moving the player according to what boolean values
//are turned on, whether left, right, up, or down

//Handle Left or Right movement
if (argument0){
  // Handle moving left
  characterMoveBlock(-player_speed);
  
  if (place_meeting(x - player_speed, y, obj_solid_parent) && place_meeting(x - player_speed, y - player_max_ramp_slope, obj_solid_parent)){
    move_contact_solid(180, player_speed);
  }
  else{
    x -= player_speed;
  }
}
else if (argument1){
  // Handle moving right
  characterMoveBlock(player_speed);
  
  if (place_meeting(x + player_speed, y, obj_solid_parent) && place_meeting(x + player_speed, y - player_max_ramp_slope, obj_solid_parent)) {
    move_contact_solid(0, player_speed);
  }
  else{
    x += player_speed;
  }
}
// Handle gravity from here on down

//See if player wants to climb flipper or ladder and return id
var climb_flipper_ladder = climbingFlipperOrLadder();

if (climb_flipper_ladder != noone && !jump_off_ladder_flipper){
  gravity = 0;
  vspeed = 0;
  canRebounce = true;
  
  if (argument2){
    // Make sure this leads to no collisions
    if (place_meeting(x, y - player_speed, obj_solid_parent)) {
      move_contact_solid(90, player_speed);
    }
    else {
      //Move UP ladder
      y -= player_speed;
    }
    //Check if at top and higher of ladder/flipper and therefore wants to jump 
    if (y + ABOVE_CLIMBING_OBJECT < climb_flipper_ladder.y){
      jump_off_ladder_flipper = true;
    }
  }
  else if (argument3){
    // Make sure this leads to no collisions
    if (place_meeting(x, y + player_speed, obj_solid_parent)) {
      move_contact_solid(270, player_speed);
    }
    else {
       //Move DOWN ladder
       y += player_speed;
    }
  }
}
else {
  // Set vspeed if they bounced on someone/something
  if (argument4 != 0) {
    vspeed = argument4;
  }
  
  // Handle normal gravity  
  // Check if they are in a ramp
  if (place_meeting(x, y, obj_solid_parent)) {
    // There is something in the way
    if (!place_meeting(x, y - player_max_ramp_slope, obj_solid_parent)) {
      // It is small enough to walk on to
      y -= player_max_ramp_slope;
      move_contact_solid(270, abs(player_max_ramp_slope));
    }
  }

  // Handle standing on button
  if (place_meeting(x, y + 2, obj_button_weight)) {
    move_contact_solid(270, 1);
    vspeed = 0;
  }
  
  //Check if in water
  if (!place_meeting(x, y, obj_water)){
    //Else handle jumping
    if ((argument2 && place_meeting(x, y + 1, obj_solid_parent)) || jump_off_ladder_flipper){
      if (jump_off_ladder_flipper){
        jump_off_ladder_flipper = false;
        vspeed = climb_object_vspeed;
      }
      else {
        vspeed = player_vspeed;
      }    
      gravity = player_gravity;
    }
  }
  
  //Handle Finishing Jumping
  if ((place_meeting(x, y + vspeed, obj_solid_parent) || place_meeting(x, y - 1, obj_solid_parent)) && vspeed < 0) {
    move_contact_solid(90, abs(vspeed));
    vspeed = 0;
  }
  else if ((place_meeting(x, y + vspeed, obj_solid_parent) || place_meeting(x, y + 1, obj_solid_parent)) && vspeed >= 0) {
    while (!place_meeting(x, y + 1, obj_solid_parent)) {
      y += 1;
    }
    
    if (place_meeting(x, y, obj_solid_parent)) {
      y -= 1;
    }
    
    vspeed = 0;
    gravity = 0;  
    canRebounce = true;
  }
  else {
    gravity = player_gravity;
  }
}
