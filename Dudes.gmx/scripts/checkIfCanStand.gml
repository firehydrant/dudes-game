/// checkIfCanStand(bool isCrouchingOn, real newSpriteAnimation )

//Last Edit 1-15-15
//Changes sprite to the one the character wants to change to and see 
//if it is a valid transformation, if not change back until it is valid

if (!argument0) {
  old_sprite_index = sprite_index;
  sprite_index = argument1;
  if (place_meeting(x, y, obj_solid_parent) && old_sprite_index == spr_char_crouch_idle){
    sprite_index = old_sprite_index;
    return true;
  }
}

return argument0;
