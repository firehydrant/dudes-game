///InitializeLevelEight()

//shortest path is 5???
numCharsInLevel = 8;

charSpawnX = 480;
charSpawnY = 127;

//Line of Break ground (left to right)
InitializeBreakGround(684, 568);
InitializeBreakGround(769, 568);
InitializeBreakGround(854, 568);
InitializeBreakGround(939, 568);
InitializeBreakGround(1024, 568);
InitializeBreakGround(1109, 568);
//Move ground in the middle
InitializeBreakGround(1279, 568);
InitializeBreakGround(1364, 568);
InitializeBreakGround(1449, 568);
InitializeBreakGround(1534, 568);
InitializeBreakGround(1619, 568);
InitializeBreakGround(1704, 568);

//Only move ground
InitializeMoveGround(1194, 568, false);

//First vertical door
InitializeButton(1219, 668, 1223, 856, spr_vertical_door, DIR_LEFT);
//Second vertical door
InitializeButton(2044, 344, 1975, 492, spr_vertical_door, DIR_LEFT);

//Only movable block
instance_create(775, 908, obj_movable_block);

//Only Weighted Ground
instance_create(2950, 1004, obj_weighted_ground);
