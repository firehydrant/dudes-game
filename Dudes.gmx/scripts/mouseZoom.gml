///mouseZoom( int amount)

var amount = 1 + argument[0];
//864

//Set Upper and Lower Zoom bounds
if (view_wview[0] <= VIEW_WIDTH && amount < 1) {
  exit;
}
if ((view_wview[0] >= (room_width - 200) || view_hview[0] >= (room_height - 200)) && amount > 1) {
  exit;
}

//Get offset to zoom from the middle
var offx = abs (view_wview[0] * amount - view_wview[0])/2;
var offy = abs (view_hview[0] * amount - view_hview[0])/2;

//Scale the view (Actually zoom)
view_wview[0] *= amount;
view_hview[0] *= amount;

//Adjust the view position based on the new scale
if (amount < 1) {
  view_xview[0] += offx;
  view_yview[0] += offy;
}
else if (amount > 1) {
  view_xview[0] -= offx;
  view_yview[0] -= offy;
}

//Make sure we are still within room boundaries after zoom
viewCheckBounds();
