///finishedTurn()

//Last Edit 11-10-15
//Checks to see if the end turn button or key was pressed 
      
return (checkPauseMenuButtons(FINISH_BUTTON_X, 
                            FINISH_BUTTON_Y, 
                            FINISH_BUTTON_X + sprite_get_width(spr_finished_turn), 
                            FINISH_BUTTON_Y + sprite_get_height(spr_finished_turn), 
                            mouse_check_button_released(mb_left)) ||
      keyboard_check_released(vk_enter));
