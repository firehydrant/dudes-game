///checkPauseMenuButtons(top_left_x, top_left_y, bottom_right_x, bottom_left_y, mouse_pressed)

var tempX = device_mouse_raw_x(0);
var tempY = device_mouse_raw_y(0);

if (tempX > argument[0] && tempY > argument[1] &&
    tempX < argument[2] && tempY < argument[3] &&
    argument[4]) {
    return true;
}

return false;
