/// savePlayerAction(PLAYER_MOVE_ACTION)

// Player is moving as PLAYER_MOVE_ACTION so save that
if (argument0 == PLAYER_MOVE_BOUNCE_NONE ||
    argument0 == PLAYER_MOVE_BOUNCE_LEFT ||
    argument0 == PLAYER_MOVE_BOUNCE_RIGHT) {
  currActionPlace += 1;
  singleTime[currActionPlace] = movedBounce;
  singleAction[currActionPlace] = argument0;
}
else if (singleAction[currActionPlace] == argument0) {
  singleTime[currActionPlace] += 1;
}
else {
  currActionPlace += 1;
  singleTime[currActionPlace] = 1;
  singleAction[currActionPlace] = argument0;
}
