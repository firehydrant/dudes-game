///handleFinishingJumping()

//Last Edit 8-27-14
//Handles the rest of the jumping process, which includes possible collisions
//with something above the character and getting him back onto solid surface


if ((place_meeting(x, y + vspeed, obj_solid) || place_meeting(x, y - 1, obj_solid)) && vspeed < 0) {
    move_contact_solid(90, abs(vspeed));
    vspeed = 0;
}

if ((place_meeting(x, y + vspeed, obj_solid) || place_meeting(x, y + 1, obj_solid)) && vspeed >= 0) {
    move_contact_solid(270, vspeed);
    vspeed = 0;
    gravity = 0;  
}
else {
    gravity = player_gravity;
}


