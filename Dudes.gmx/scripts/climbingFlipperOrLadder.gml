/// climbingFlipperOrLadder

//Last Edit 5-2-15
//This script is making sure we want to climb something, 
//whether its a ladder a flipper, so first it checks to see the 
//player is in a ladder and return that, if not that check to see if in flipper

//Check if in ladder
var ladder_inst = instance_place(x, y, obj_ladder);

if (ladder_inst != noone){
  return ladder_inst;
}

//Else if check if on flipper
var flipper_inst = instance_place(x + 1, y, obj_flipper);

if (flipper_inst == noone) {
  flipper_inst = instance_place(x - 1, y, obj_flipper);
}

if (flipper_inst != noone) {
  if (flipper_inst.down){
    return flipper_inst;
  }
}
//Else return negative for both ladder and flipper
return noone;

