with (obj_button_weight) {
  instance_destroy();
}

with (obj_activatable_parent) {
  instance_destroy();
}

with (obj_movable_block) {
  instance_destroy();
}

with (obj_weighted_ground) {
  instance_destroy();
}

with (obj_move_ground) {
  instance_destroy();
}

with (obj_flipper) {
  instance_destroy();
}

with (obj_break_ground) {
  instance_destroy();
}
