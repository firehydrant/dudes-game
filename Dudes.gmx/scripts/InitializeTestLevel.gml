/// InitializeTestLevel()

numCharsInLevel = 5;
charSpawnX = 275;
charSpawnY = 528;


instance_create(925, 216, obj_weighted_ground);
InitializeMoveGround(700, 176, false);

/*
//Fixing Flipper Bug where could open even if there was an object that
//it would collide with
InitializeFlipper(650, 576, false);
InitializeMoveGround(500, 656, true);
//InitializeMoveGround(600, 656, true);
*/

//test move ground
InitializeMoveGround(750, 736, true);

//Wall of Break Ground
InitializeBreakGround(150, 740);
InitializeBreakGround(150, 760);
InitializeBreakGround(150, 780);


//Testing 11th level stuff
instance_create(254, 837, obj_weighted_ground);
InitializeMoveGround(625, 832, false);
InitializeMoveGround(25, 904, true);

//InitializeFlipper(850, 624, true);

//Testing 12th level Stuff
InitializeFlipper(1300, 488, false);
InitializeFlipper(1225, 384, true);


InitializeBreakGround(250, 192);

//Steps up
InitializeBreakGround(100, 536);
InitializeBreakGround(250, 488);
InitializeBreakGround(100, 416);
InitializeBreakGround(250, 360);

//Path to run accross
InitializeBreakGround(975, 832);
InitializeBreakGround(1075, 832);
InitializeBreakGround(1175, 832);
InitializeBreakGround(1275, 832);

InitializeFlipper(750, 568, true);
