///exitIfPaused()

if (global.PAUSE) {
  show_debug_message("PAUSING");
  exit;
}
