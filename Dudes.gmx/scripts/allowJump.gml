///allowJump()

//Last Edit 11-10-15
//Checks to see if the allow jump button or key was pressed 

return (checkPauseMenuButtons(ALLOW_JUMP_BUTTON_X, 
                            ALLOW_JUMP_BUTTON_Y, 
                            ALLOW_JUMP_BUTTON_X + sprite_get_width(spr_allow_jump), 
                            ALLOW_JUMP_BUTTON_Y + sprite_get_height(spr_allow_jump), 
                            device_mouse_check_button(0, mb_left)) ||
      keyboard_check(vk_alt));
      
